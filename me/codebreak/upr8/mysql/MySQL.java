package me.codebreak.upr8.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MySQL {

	private Connection connection;
	
	public MySQL(String ip, String userName, String password, String db) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + ip + "/" + db + "?user=" + userName + "&password=" + password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public ArrayList<String> getPlayers(){
		ArrayList<String> playernames = new ArrayList<String>();
		try {
            PreparedStatement statement = connection.prepareStatement("select Username from userdata;");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	playernames.add(result.getString("Username"));
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return playernames;
	}
	
	public String getIP(String p){
		try {
            PreparedStatement statement = connection.prepareStatement("select IP from userdata where Username='" + p + "';");
            ResultSet result = statement.executeQuery();
           
            if(result.next()){
            	return result.getString("IP");
            }else{
            	return null;
            }
		} catch (Exception e) {
            e.printStackTrace();
            return "[[Failed to connect]]";
		}
	}
	///
	public String getUUID(String p){
		try {
            PreparedStatement statement = connection.prepareStatement("select UUID from userdata where Username='" + p + "';");
            ResultSet result = statement.executeQuery();
           
            if(result.next()){
            	return result.getString("UUID");
            }else{
            	return null;
            }
		} catch (Exception e) {
            e.printStackTrace();
            return "[[Failed to connect]]";
		}
	}
	
	public Date getLastOnline(String p){
		try {
            PreparedStatement statement = connection.prepareStatement("select LastOnline from userdata where Username='" + p + "';");
            ResultSet result = statement.executeQuery();
           
            if(result.next()){
            	java.sql.Date date = java.sql.Date.valueOf(result.getString("LastOnline"));
            	return date;
            }else{
            	return null;
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
	}
	
	public int getPlaytime(String p){
		try {
            PreparedStatement statement = connection.prepareStatement("select Playtime from userdata where Username='" + p + "';");
            ResultSet result = statement.executeQuery();
           
            if(result.next()){
            	return result.getInt("Playtime");
            }else{
            	return 0;
            }
		} catch (Exception e) {
            e.printStackTrace();
            return 0;
		}
	}
	
	public int getLogins(String p){
		try {
            PreparedStatement statement = connection.prepareStatement("select Logins from userdata where Username='" + p + "';");
            ResultSet result = statement.executeQuery();
           
            if(result.next()){
            	return result.getInt("Logins");
            }else{
            	return 0;
            }
		} catch (Exception e) {
            e.printStackTrace();
            return 0;
		}
	}
	
	public void createTable() throws SQLException {
	    String sqlCreate = "create table if not exists userdata (Username text, UUID varchar(40), Playtime int, LastOnline text, IP text, Logins int);";

	    PreparedStatement stmt = (PreparedStatement) connection.createStatement();
	    stmt.execute(sqlCreate);
	}
	
	
	
	public void addUser(Player p, int Playtime) throws SQLException {
		Date now = new Date();
		java.sql.Date date = new java.sql.Date(now.getTime());
		
		String sqlCreate = "insert into userdata (Username, UUID, Playtime, LastOnline, IP, Logins) values ('" + p.getName() + "', '" + p.getUniqueId().toString() + "', " + Playtime + ", '" + date.toString() + "', '" + p.getAddress().toString() + "', 0);";

	    PreparedStatement stmt = (PreparedStatement) connection.createStatement();
	    stmt.execute(sqlCreate);
	}
	
	public void updateUser(Player p, int Playtime, int logins) throws SQLException {
		Date now = new Date();
		java.sql.Date date = new java.sql.Date(now.getTime());
		
		String sqlCreate = "update userdata set Username='" + p.getName() + "', Playtime=" + Playtime + ", LastOnline='" + date.toString() + "', IP='" + p.getAddress().toString() + "', Logins=" + logins + " where UUID='" + p.getUniqueId().toString() + "';";

	    PreparedStatement stmt = (PreparedStatement) connection.createStatement();
	    stmt.execute(sqlCreate);
	}
	
	public void update(){
		for(Player p : Bukkit.getServer().getOnlinePlayers()){
			int Playtime = getPlaytime(p.getName()) + 1;
			int logins = getLogins(p.getName()) + 1;
			try {
				updateUser(p, Playtime, logins);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
