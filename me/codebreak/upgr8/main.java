package me.codebreak.upgr8;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.codebreak.upgr8.Listeners.JoinEvent;
import me.codebreak.upgr8.user.userdata;
import me.codebreak.upr8.mysql.MySQL;
import me.codebreak.upr8.mysql.SettingsManager;

public class main extends JavaPlugin implements Listener{
	
	private static MySQL mysql;
	
	@Override
	public void onEnable() {
		getCommand("gm").setExecutor(new commands());
		getCommand("day").setExecutor(new commands());
		getCommand("night").setExecutor(new commands());
		getCommand("userdata").setExecutor(new userdata());
		getCommand("heal").setExecutor(new commands());
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new JoinEvent(), this);
		
		//Bukkit.getServer().getLogger().info(main.getMysql().getConfigString1(Wat moet hier?));
		
		connectMysql();
	 }
	
	@Override
	public void onDisable() {	
		
	}
	
	public static Plugin getPlugin(){
		return Bukkit.getServer().getPluginManager().getPlugin("Upgr8");
	}
	
	public void connectMysql(){
		ConfigurationSection section = SettingsManager.getConfig().<ConfigurationSection>get("MySQL");
		if(!SettingsManager.getConfig().contains("MySQL.Host")){
			SettingsManager.getConfig().createSection("MySQL");
			SettingsManager.getConfig().save();
			
			ConfigurationSection s = SettingsManager.getConfig().<ConfigurationSection>get("MySQL");
			s.createSection("Host");
			s.createSection("User");
			s.createSection("Password");
			s.createSection("Database");
			SettingsManager.getConfig().save();
			return;
		}
		
		if(section.getString("Host") == null || section.getString("User") == null || section.getString("Password") == null || section.getString("Database") == null){
			return;
		}
		
		String ip = section.getString("Host");
		String username = section.getString("User");
		String password = section.getString("Password");
		String db = section.getString("Database");
		
		mysql = new MySQL(ip, username, password, db);
		try {
			mysql.createTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Succesfully connected to mysql database!");
		
		Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
				mysql.update();
			}
		}, 0, 800);
	}
	
	public static MySQL getMysql(){
		return mysql;
	}
}