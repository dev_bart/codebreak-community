package me.codebreak.upgr8.Listeners;

import java.sql.SQLException;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.codebreak.upgr8.main;

public class JoinEvent implements Listener{

		@EventHandler
		public void onPlayerJoin(PlayerJoinEvent e){
			if(main.getMysql().getPlayers().isEmpty()){
				try {
					main.getMysql().addUser(e.getPlayer(), 0);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}else{
				if(main.getMysql().getPlayers().contains(e.getPlayer().getName())){
					try {
						main.getMysql().updateUser(e.getPlayer(), main.getMysql().getPlaytime(e.getPlayer().getName()) + 1, main.getMysql().getLogins(e.getPlayer().getName()) + 1);
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}else
						try {
							main.getMysql().addUser(e.getPlayer(), 0);
						} catch (SQLException e1) {
							e1.printStackTrace();
						}					
					
				
			}
		}
}
