package me.codebreak.upgr8.user;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.codebreak.upgr8.main;
import net.md_5.bungee.api.ChatColor;

public class userdata implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			sender.sendMessage("Don't run this command on console!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(label.equalsIgnoreCase("userdata")){
			if(p.hasPermission("codebreak.commands.userdata")){
				if (args.length == 0) {
					p.sendMessage(ChatColor.RED + "Please specify a player");
					return true;
				}
				
				Player target = Bukkit.getServer().getPlayer(args[0]);
				
				if(target == null){
					if(!main.getMysql().getPlayers().contains(args[0])){
						p.sendMessage(ChatColor.RED + "We don't have any data of that player!");
						return true;
					}
					p.sendMessage(ChatColor.GREEN + "Userdata of " + args[0]);
					p.sendMessage(ChatColor.GREEN + "UUID: " + main.getMysql().getUUID(args[0]));
					p.sendMessage(ChatColor.GREEN + "IP: " + main.getMysql().getIP(args[0]));
					p.sendMessage(ChatColor.GREEN + "Logins: " + main.getMysql().getLogins(args[0]));
					p.sendMessage(ChatColor.GREEN + "Playtime: " + main.getMysql().getPlaytime(args[0]) + ChatColor.GREEN + " minutes");
					p.sendMessage(ChatColor.GREEN + "Last online (dd/mm/jj): " + main.getMysql().getLastOnline(args[0]));
					return true;
				}
				
				if (args.length == 1) {
					p.sendMessage(ChatColor.GREEN + "Userdata of " + target.getName());
					p.sendMessage(ChatColor.GREEN + "UUID: " + target.getUniqueId());
					p.sendMessage(ChatColor.GREEN + "IP: " + main.getMysql().getIP(target.getName()));
					p.sendMessage(ChatColor.GREEN + "Logins: " + main.getMysql().getLogins(target.getName()));
					p.sendMessage(ChatColor.GREEN + "Playtime: " + main.getMysql().getPlaytime(target.getName()) + ChatColor.GREEN + " minutes");
					p.sendMessage(ChatColor.GREEN + "Last online (dd/mm/jj): " + main.getMysql().getLastOnline(target.getName()));
					return true;
				}
				
				if (args.length == 2 || args.length == 3 || args.length == 4){
					p.sendMessage(ChatColor.RED + "Too many arguments!");
					return true;
				}
			}
			p.sendMessage(ChatColor.RED + "You don't have access to perform this command!");
			return true;	
		}
		return true;	
	}
}
//jgirjrrg
