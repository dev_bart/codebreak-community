package me.codebreak.upgr8;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			sender.sendMessage(ChatColor.RED + "You can't use these commands!");
			return true;
		}
		
		Player p = (Player) sender;
		
		if(label.equalsIgnoreCase("gm")){
			if(args.length == 0){
				p.sendMessage(ChatColor.RED + "Please use /gm <GameMode> (PlayerName)!");
				return true;
			}
			
			else if(args.length == 1){
				if(args[0].equalsIgnoreCase("creative") ||
				   args[0].equalsIgnoreCase("c") ||
				   args[0].equalsIgnoreCase("1")){
					p.setGameMode(GameMode.CREATIVE);
					p.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
					return true;
				}
				else if(args[0].equalsIgnoreCase("survival") ||
						   args[0].equalsIgnoreCase("s") ||
						   args[0].equalsIgnoreCase("0")){
							p.setGameMode(GameMode.SURVIVAL);
							p.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
							return true;
						}
				else if(args[0].equalsIgnoreCase("adventure") ||
						   args[0].equalsIgnoreCase("a") ||
						   args[0].equalsIgnoreCase("2")){
							p.setGameMode(GameMode.ADVENTURE);
							p.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
							return true;
						}
				else if(args[0].equalsIgnoreCase("spectator") ||
						   args[0].equalsIgnoreCase("spec") ||
						   args[0].equalsIgnoreCase("3")){
							p.setGameMode(GameMode.SPECTATOR);
							p.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
							return true;
						}
				else{
					p.sendMessage(ChatColor.RED + "Invalid gamemode!");
					return true;
				}
			}
			
			else if(args.length == 2){
				Player p2 = Bukkit.getServer().getPlayer(args[1]);
				if(p2 == null){
					p.sendMessage(ChatColor.RED + "That player isn't online!");
					return true;
				}
				else{
					if(args[0].equalsIgnoreCase("creative") ||
							   args[0].equalsIgnoreCase("c") ||
							   args[0].equalsIgnoreCase("1")){
								p2.setGameMode(GameMode.CREATIVE);
								p2.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
								p.sendMessage(ChatColor.GREEN + args[1] + "'s gamemode has been updated!");
								return true;
							}
							else if(args[0].equalsIgnoreCase("survival") ||
									   args[0].equalsIgnoreCase("s") ||
									   args[0].equalsIgnoreCase("0")){
										p2.setGameMode(GameMode.SURVIVAL);
										p2.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
										p.sendMessage(ChatColor.GREEN + args[1] + "'s gamemode has been updated!");
										return true;
									}
							else if(args[0].equalsIgnoreCase("adventure") ||
									   args[0].equalsIgnoreCase("a") ||
									   args[0].equalsIgnoreCase("2")){
										p2.setGameMode(GameMode.ADVENTURE);
										p2.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
										p.sendMessage(ChatColor.GREEN + args[1] + "'s gamemode has been updated!");
										return true;
									}
							else if(args[0].equalsIgnoreCase("spectator") ||
									   args[0].equalsIgnoreCase("spec") ||
									   args[0].equalsIgnoreCase("3")){
										p2.setGameMode(GameMode.SPECTATOR);
										p2.sendMessage(ChatColor.GREEN + "You gamemode has been updated!");
										p.sendMessage(ChatColor.GREEN + args[1] + "'s gamemode has been updated!");
										return true;
									}
							else{
								p.sendMessage(ChatColor.RED + "Invalid gamemode!");
								return true;
							}
				}
			}
			else{
				p.sendMessage(ChatColor.RED + "Please use /gm <GameMode> (PlayerName)!");
				return true;
			}
		}
		
		else if(label.equalsIgnoreCase("day")){
			p.getWorld().setTime(0);
			p.sendMessage(ChatColor.GREEN + "Time set to day!");
			return true;
		}
		
		else if(label.equalsIgnoreCase("night")){
			p.getWorld().setTime(18000);
			p.sendMessage(ChatColor.GREEN + "Time set to night!");
			return true;
		}
	
		return true;
	}
}
